# README


## Getting Started
- `npm i` -> Install the required packages
- `npm run start` -> Will start the server on 5050 port
- The server will start running at: `http://localhost:5050`


## HTML pages
- `http://localhost:5050/submit-kyc` -> Submit details for a user
- `http://localhost:5050/kyc-pending` -> Shows list of KYCs pending for approval
- `http://localhost:5050/kyc-completed` -> Shows list of completed KYCs


## API Endpoints

- *POST (JSON)*  **/kyc**: Submit new KYC
- *PUT (JSON)*  **/kyc**: Update KYC

- *GET*  **/kyc**: All completed KYCs

- *GET* **/kyc/pending**: Returns all KYCs pending for approval

