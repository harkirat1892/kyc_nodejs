let responder = require("../helpers/common"),
    errorResponder = responder.errorResponse;


let controllerObj = {
    init: function(toImportObjs){},


    // Acts as a middleware, next() is very important to continue
    // Stops unauthorized requests
    handleApiAuthorization: function (req, res, next) {
        // Try to get token from POST request's data, header and GET param
        let token = req.header("Authorization") || req.body.token || req.query.token;

        // Proper validation from DB required here in production

        if(token && token.trim() !== "" && token === "iamauthenticated") {
            req.authenticated = true;

            next();
        } else {
            // Block unauthenticated users
            return errorResponder(res);
        }
    }

};


module.exports = function(toImportObjs) {
    controllerObj.init(toImportObjs);

    return controllerObj;
};