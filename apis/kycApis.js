let self = this,
    async = require("async"),
    _ = require("underscore"),
    Joi = require("joi");

let common = require("../helpers/common"),
    successResponder = common.successResponse,
    errorResponder = common.errorResponse;

const InvalidArgumentException = require("../kyc/exceptions").InvalidArgumentException;

let kycInteractor = require("../kyc/interactors");
const apiValidators = require("../kyc/apiValidators");

const fileUploadsPath = __dirname + '/../public/user_uploads/';


let controllerObj = {

    init: function(toImportObjs){},


    handleAddKyc: function (req, res) {
        let data = req.body,
            validate = Joi.validate(data, apiValidators.newKyc);

        if (validate.error){
            return errorResponder(res, validate.error.details[0].message);
        }

        let passportImage = req.files.passport,
            bankStatementImage = req.files.bankStatement;

        if (!passportImage || !bankStatementImage){
            console.log("passportImage: ", passportImage);
            console.log("bankStatementImage: ", bankStatementImage);

            throw new InvalidArgumentException("Required files not provided");
        }

        let resp = {},
            passportLocalPath = "",
            bankStatementLocalPath = "";

        async.parallel([
            function (cb) {
                let randString = common.getRandomString();
                passportLocalPath = fileUploadsPath + randString + '-' + passportImage.name;

                passportImage.mv(passportLocalPath, function(err) {
                    if (err) {
                        console.log("err: ", err);
                        throw new InvalidArgumentException("Error while saving passport");
                    } else
                        cb();
                });
            },

            function (cb) {
                let randString = common.getRandomString();
                bankStatementLocalPath = fileUploadsPath + randString + '-' + passportImage.name;

                bankStatementImage.mv(bankStatementLocalPath, function(err) {
                    if (err)
                        throw new InvalidArgumentException("Error while saving bank statement");
                    else
                        cb();
                });
            }
        ], function (mainErr) {
            if(mainErr){
                throw new InvalidArgumentException("Error while uploading files");
            } else{
                let queryData = validate.value;
                queryData.bankStatementPath = bankStatementLocalPath;
                queryData.passportLocalPath = passportLocalPath;

                // Adds to DB
                kycInteractor.addKyc(queryData, function(e, resp){
                    return successResponder(res, resp);
                });
            }
        });
    },

    handleApproveKyc: function(req, res) {
        let data = req.body,
            validate = Joi.validate(data, apiValidators.approveKyc);

        if (validate.error){
            return errorResponder(res, validate.error.details[0].message);
        }

        let queryData = validate.value;

        kycInteractor.approveKyc(queryData, function(e){
            return successResponder(res);
        });

    },

    handleGetPendingKycs: function(req, res) {
        kycInteractor.getPendingKycs(function (e, data) {
            return successResponder(res, data);
        });
    },

    handleGetApprovedKycs: function(req, res) {
        kycInteractor.getCompletedKycs(function(e, resp){
            return successResponder(res, resp);
        });
    }

};


module.exports = function(toImportObjs) {
    controllerObj.init(toImportObjs);

    return controllerObj;
};