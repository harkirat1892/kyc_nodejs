const express = require('express'),
    path = require('path'),
    // favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    toobusy = require('toobusy-js'),
    fileUpload = require('express-fileupload');

// const router = express.Router();
// Default env file
let envFilePath = ".envDevelopment";
console.log("Assuming NODE_ENV to be development, using " + envFilePath);
console.log("process.env.NODE_ENV is: ", process.env.NODE_ENV);

if (process.env.NODE_ENV === 'production'){
    envFilePath = ".env";

    console.log("NODE_ENV is production, using " + envFilePath);
}
require('dotenv').config({path: envFilePath});


const mongoUtil = require('./models/mongodb/mongoUtil');


// Express app
const app = express();

app.mongoConnection = mongoUtil.db;


// middleware which blocks requests when we're too busy
app.use(function(req, res, next) {
    if (toobusy()) {
        res.status(503).send("I'm busy right now, sorry.");
    } else {
        next();
    }
});

app.enable("trust proxy"); // only if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)

// Disables "x-powered-by" header set by Express
app.disable('x-powered-by');

// view engine setup
// @TODO: Remove if no view required
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

logger('combined');

app.use(logger('dev'));
app.use(fileUpload({
    createParentPath: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.options("*",function(req, res, next) {
    res.header("Access-Control-Allow-Origin", req.get("Origin") || "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Auth");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");

    res.status(200).end();
});

app.use("*",function(req, res, next){
    res.header("Access-Control-Allow-Origin", req.get("Origin")||"*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// Collection instantiations

// ***** Initialise collection instances with mongodb connection *****
let collections = {
    incomeUpdateLogsModel: require('./models/mongodb/collections/logs/analytics')(mongoUtil),
    analyticsModel: require('./models/mongodb/collections/users/usersModel')(mongoUtil)
};


// ******* Load and initialise controllers, pass collections obj to them
const authApis = require('./apis/authApis')(collections),
    kycApis = require('./apis/kycApis')(collections);


// ************************* Routes Begin *************************** //

// Authorization check functions
const authCheckFunction = authApis.handleApiAuthorization.bind(this);

// All authenticated requests should first go through authCheckFunction
// It sets values in req object


// API endpoints
app.get('/kyc', [kycApis.handleGetApprovedKycs.bind(this)]);

app.post('/kyc', [authCheckFunction, kycApis.handleAddKyc.bind(this)]);
app.put('/kyc', [authCheckFunction, kycApis.handleApproveKyc.bind(this)]);

app.get('/kyc/pending', [authCheckFunction, kycApis.handleGetPendingKycs.bind(this)]);


// HTML pages
app.get('/submit-kyc', function (req, res) {
    res.sendFile(__dirname + '/views/submit-kyc.html');
});

app.get('/kyc-pending', function (req, res) {
    res.sendFile(__dirname + '/views/kyc-pending.html');
});

app.get('/kyc-completed', function (req, res) {
    res.sendFile(__dirname + '/views/kyc-completed.html');
});

// ************************ Routes End **************************** //


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  console.log("Error in error handler: ", err.message);
  // render the error page
  res.status(err.status || 500);

  if (err.message) {
      res.json({message: err.message});
  } else{
      res.json({});
  }
});

module.exports = app;
