const _ = require("underscore");


module.exports = {

    successResponse: function(res, objToReturn, internalDump) {
        let responseJson = {};

        if (objToReturn && typeof objToReturn === 'object') {
            responseJson = _.extend(responseJson, objToReturn);
        }

        // If internalDump, dump it
        if (internalDump) {
            console.log("Error internal dump: ", internalDump);
        }

        let statusCode = 200;
        res.status(statusCode);

        return res.json(responseJson);
    },

    errorResponse: function(res, responderObj, internalDump) {
        // Setting status code to: "Bad request"
        let statusCode = 400,
            responseText = "Bad request";

        let responseJson = {message: responseText};

        if (typeof responderObj === 'object') {
            // New method
            console.log("Using newer error responder, with object: ", responderObj);
            internalDump = responderObj.internalDump;
            statusCode = responderObj.statusCode || statusCode;

            // Foolish, have to support messageText too till all its uses vanish
            responseJson.message = responseText;

            let returnObj = responderObj.returnObj;
            if (returnObj) {
                responseJson = _.extend(responseJson, returnObj);
            }
        } else {
            responseJson.message = responderObj;
        }

        // If internalDump, dump it
        if (internalDump) {
            console.log("Error internal dump: ", internalDump);
        } else {
            console.log("Responding: ", responseText);
        }

        res.status(statusCode);
        return res.json(responseJson);
    },

    getRandomString: function (length) {
        if (!length)
            length = 10;

        // Used rarely in this controller, so not declaring (requiring) globally
        let crypto = require("crypto");

        // 1 byte roughly has 2 letters!
        let randomString = crypto.randomBytes(Math.ceil(length/2)).toString('hex');

        console.log("randomString is: ", randomString);
        return randomString;
    }
};
