const Joi = require("joi");


module.exports = {

    newKyc: {
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        address: Joi.string().required(),
        nationality: Joi.string().required()
    },

    approveKyc: {
        kycId: Joi.string().required()
    }

};
