const Enum = require('enum');


let userStatuses = new Enum({
    notSubmitted: "Not Submitted",
    pending: "Pending",
    approved: "Approved",
});


module.exports = {
    userStatuses
};
