const _ = require("underscore");
const async = require('async');

const InvalidArgumentException = require("./exceptions").InvalidArgumentException;

const mongoUtil = require('../models/mongodb/mongoUtil');
const userStatuses = require("./constants").userStatuses;
let analyticsModel = require('../models/mongodb/collections/logs/analytics')(mongoUtil),
    usersModel = require('../models/mongodb/collections/users/usersModel')(mongoUtil);


module.exports = {

    addKyc: function(inputData, callback) {
        inputData.kycStatus = userStatuses.pending.key;

        usersModel.insertAndReturnId(inputData, function(e, insertedId) {
            if(e){
                console.log("Error: ", e);
                throw new InvalidArgumentException("Internal error happened");
            } else{
                return callback(false, {insertedId});
            }
        });
    },

    approveKyc: function(data, callback) {
        let updateTo = {kycStatus: userStatuses.approved.key};
        //We can also put up validations to check first if this kyc object is already approved

        let kycId = data.kycId;

        async.waterfall([
            function(cb) {
                usersModel.updateOneById(kycId, updateTo, function(e, data) {
                    if (e){
                        console.log("error: ", e);
                        throw new InvalidArgumentException("Failed while updating KYC");
                    } else{
                        cb();
                    }
                });
            }
        ], function(mastErr) {
            if(mastErr){
                console.log("Error: ", mastErr);
                throw new InvalidArgumentException("Internal error");
            } else{
                //Update successful
                return callback();
            }
        });
    },

    getPendingKycs: function(callback) {
        let pendingKycs = [];
        let condition = {kycStatus: userStatuses.pending.key},
            sortOptions = {
                sort: {
                    '_id': -1
                }
            },
            selectFields = {};

        async.waterfall([

            function(cb) {
                usersModel.findAll(condition, selectFields, sortOptions, function(e, kycs) {
                    if (e){
                        console.log("error: ", e);
                        throw new InvalidArgumentException("Failed while fetching pending KYCs");
                    } else{
                        pendingKycs = kycs;
                        console.log("kycs: ", kycs);
                        cb();
                    }
                });
            }
        ], function(mastErr) {
            if(mastErr){
                console.log("Error: ", mastErr);
                throw new InvalidArgumentException("Internal error");
            } else{
                // Search successful
                return callback(false, {kycs: pendingKycs});
            }
        });
    },

    getCompletedKycs: function(callback) {
        let approvedKycs = [];
        let condition = {kycStatus: userStatuses.approved.key},
            sortOptions = {
                sort: {
                    '_id': -1
                }
            },
            selectFields = {};

        async.waterfall([

            function(cb) {
                usersModel.findAll(condition, selectFields, sortOptions, function(e, kycs) {
                    if (e){
                        console.log("error: ", e);
                        throw new InvalidArgumentException("Failed while fetching");
                    } else{
                        approvedKycs = kycs;
                        cb();
                    }
                });
            }
        ], function(mastErr) {
            if(mastErr){
                console.log("Error: ", mastErr);
                throw new InvalidArgumentException("Internal error");
            } else{
                // Search successful
                return callback(false, {kycs: approvedKycs});
            }
        });
    }

};
