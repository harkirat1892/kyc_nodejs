const _ = require("underscore"),
    async = require("async");

// Constructor, which takes in app, which helps initialise db from the common mongo db in app.js
module.exports = function (self) {
    if(!self){
        throw Error("BaseModel wasn't initialised!");
    }

    return {
        collection: false,
        updateUpdatedAt: true,
        insertCreatedAt: true,


        // If ID is already an Object, let it go
        getObjectId: function(objId) {
            try {
                if (self.ObjectID.isValid(objId)) {
                    try{
                        objId = new self.ObjectID(objId);

                        return objId;
                    } catch (e){
                        return objId;
                    }
                } else {
                    objId = new self.ObjectID(objId);

                    return objId;
                }
            } catch (err) {
                return false;
            }
        },


        _parseData: function(data) {
            if (this.parseInput){
                // Each model can have custom validations by implementing "parseInput"
                data = this.parseInput(data);
            } else{
                console.log("No parsing set up");
            }

            // Common parsing here
            if ("amount" in data){
                data.amount = parseFloat(data.amount);
            }
            if ("nature" in data){
                data.nature = data.nature.toLowerCase();
            }

            return data;
        },


        insert: function (data, callback) {
            if (_.isEmpty(data))
                return false;

            data = this._parseData(data);

            let dateNow = new Date();
            if (self.insertCreatedAt !== false){
                console.log("Inserting creation time..");
                data.createdAt = dateNow;
            }
            if (self.updateUpdatedAt !== false){
                console.log("Inserting update time..");
                data.updatedAt = dateNow;
            }

            if (callback) {
                return self.collection.insertOne(data, callback);
            } else {
                return self.collection.insertOne(data);
            }
        },


        insertAndReturnId: function (data, callback) {
            if (_.isEmpty(data)) {
                if (callback)
                    return callback("Empty data to insert?");
                else
                    return false;
            }

            data = this._parseData(data);

            let dateNow = new Date();

            if (self.insertCreatedAt !== false){
                console.log("Inserting creation time..");
                data.createdAt = dateNow;
            }
            if (self.updateUpdatedAt !== false){
                console.log("Inserting update time..");
                data.updatedAt = dateNow;
            }

            if (callback) {
                self.collection.insertOne(data, function (err, insertData) {
                    if (!err && insertData.insertedCount && insertData.insertedId) {
                        return callback(err, insertData.insertedId);
                    } else {
                        return callback("Insert failed");
                    }
                });
            } else {
                return self.collection.insertOne(data);
            }
        },


        updateOneById: function (id, updateData, callback) {
            id = this.getObjectId(id);

            if (!id) {
                if (callback){
                    return callback("Invalid ID");
                } else
                    return false;
            }

            if (_.isEmpty(updateData)) {
                console.log("Data to update is null");
                return false;
            }

            updateData = this._parseData(updateData);

            if (self.updateUpdatedAt !== false){
                updateData.updatedAt = new Date();
            }

            // If "$set" not set, sets the data keys for update
            for (let key in updateData) {
                if (key.indexOf("$") < 0) {
                    updateData = {"$set": updateData};
                    break;
                } else{
                    break;
                }
            }

            if (callback) {
                return self.collection.updateById(id, updateData, callback);
            } else {
                return self.collection.updateById(id, updateData);
            }
        },


        updateOneByCondition: function (findCond, updateData, callback) {
            if(findCond._id){
                let id = this.getObjectId(findCond._id);

                if (!id) {
                    if (callback)
                        return callback("Invalid ID");
                    else
                        return false;
                } else{
                    console.log("Modified _id into object!");
                    findCond["_id"] = id;
                }
            }

            if (_.isEmpty(updateData)) {
                console.log("Data to update is null");
                return callback("Data to update is null");
            }

            updateData = this._parseData(updateData);

            if (self.updateUpdatedAt){
                updateData.updatedAt = new Date();
            }

            // If "$set" not set, sets the data keys for update
            for (let key in updateData) {
                if (key.indexOf("$") < 0) {
                    updateData = {"$set": updateData};
                    break;
                } else{
                    break;
                }
            }

            // Crap, updates one doc only, even if findCond has multiple docs satisfying the condition
            self.collection.findOneAndUpdate(findCond, updateData, {returnOriginal: false}, function(err, updatedDoc){
                if(err){
                    console.log("Error is: ", err);
                    return callback("Error");
                } else{
                    // if(data.lastErrorObject && data.lastErrorObject.updatedExisting) {
                    //     return callback(null, data.value._id);
                    // } else {
                    //     return callback(null, data.lastErrorObject.upserted);
                    // }
                    return callback(false, updatedDoc);
                }
            });
        },


        updateMultipleByCondition: function (findCond, updateData, callback) {
            if(findCond._id){
                let id = this.getObjectId(findCond._id);

                if (!id) {
                    if (callback)
                        return callback("Invalid ID");
                    else
                        return false;
                } else{
                    console.log("Modified _id into object!");
                    findCond["_id"] = id;
                }
            }

            if (_.isEmpty(updateData)) {
                console.log("Data to update is null");
                return callback("Data to update is null");
            }

            updateData = this._parseData(updateData);

            if (self.updateUpdatedAt){
                updateData.updatedAt = new Date();
            }

            // If "$set" not set, sets the data keys for update
            for (let key in updateData) {
                if (key.indexOf("$") < 0) {
                    updateData = {"$set": updateData};
                    break;
                } else{
                    break;
                }
            }

            // Crap, updates one doc only, even if findCond has multiple docs satisfying the condition
            self.collection.updateMany(findCond, updateData, function(err, data){
                if(err){
                    console.log("Error is: ", err);
                    return callback("Error");
                } else{
                    console.log("Updated many: ", data);
                    return callback();
                }
            });
        },


        findOneById: function (id, callback) {
            id = this.getObjectId(id);

            if (!id) {
                if (callback) {
                    return callback("Invalid ID");
                } else
                    return false;
            }
            let findCond = {"_id": id};

            if (callback) {
                return self.collection.findOne(findCond, callback);
            } else {
                return self.collection.findOne(findCond);
            }
        },


        findOne: function (findCond, callback) {
            if(findCond._id){
                let id = this.getObjectId(findCond._id);

                if (!id) {
                    if (callback)
                        callback("Invalid ID");
                    else
                        return false;
                } else{
                    findCond._id = id;
                }
            }

            if (callback) {
                return self.collection.findOne(findCond, callback);
            } else {
                return self.collection.findOne(findCond);
            }
        },


        /*
            selectFields to avoid _id: {_id: 0}
            to include _id {_id: 1}
         */

        // let otherOptionsObj = {
        //     limit: limit,
        //     skip: skip,
        //     sort: {
        //         '_id': -1
        //     }
        // };

        //-1 in sort means descending
        findAll: function (findCond, selectFields, otherOptionsObj, callback) {
            if(!callback && typeof selectFields === "function"){
                // Pretty common mistake
                callback = selectFields;

                // Get all fields
                selectFields = {};
            }

            if(!callback && typeof otherOptionsObj === "function"){
                callback = otherOptionsObj;
                otherOptionsObj = {};
            }

            if(!otherOptionsObj)
                otherOptionsObj = {};

            return self.collection.find(findCond, selectFields, otherOptionsObj).toArray(callback);
        },


        getArrayOfObjectIds: function (idsArray, callback) {
            let thisSelf = this,
                objIdsArray = [];

            // Convert ids into Objects
            async.each(idsArray,
                function (id, doneCb) {
                    id = thisSelf.getObjectId(id);

                    if (!id) {
                        console.log("Invalid ID");
                        doneCb();
                    } else{
                        objIdsArray.push(id);
                        doneCb();
                    }
                },
                function (err) {
                    if (err) {
                        console.log("Error: ", err);
                        return callback("Error");
                    } else {
                        return callback(null, objIdsArray);
                    }
                }
            );
        },


        findFromArrayOfIds: function (idsArray, convertToObjs, callback) {
            let thisSelf = this;

            if(convertToObjs) {
                let objIdsArray = [];

                // Convert ids into Objects
                async.each(idsArray,
                    function (id, doneCb) {
                        id = thisSelf.getObjectId(id);

                        if (!id) {
                            console.log("Invalid ID");
                            doneCb();
                        } else{
                            objIdsArray.push(id);
                            doneCb();
                        }
                    },
                    function (err) {
                        if (err) {
                            console.log("Error: ", err);
                            return callback("Error");
                        } else {
                            return self.collection.find({"_id": {$in: objIdsArray}}, {}).toArray(callback);
                        }
                    });
            } else{
                return self.collection.find({"_id": {$in: idsArray}}, {}).toArray(callback);
            }
        },


        // Insert only when not present otherwise update
        insertOrUpdate: function(findCond, updateData, callback){
            if (_.isEmpty(findCond)) {
                console.log("find Condition is  empty");
                return false;
            }

            if (_.isEmpty(updateData)) {
                console.log("Data to update is null");
                return false;
            }

            updateData = this._parseData(updateData);

            if (self.updateUpdatedAt){
                updateData.updatedAt = new Date();
            }

            for (let key in updateData) {
                if (key.indexOf("$") < 0) {
                    updateData = {"$set": updateData};
                    break;
                } else{
                    break;
                }
            }

            self.collection.findOneAndUpdate(findCond, updateData, {upsert: true}, function(err, data){
                if(err){
                    console.log("Error is: ", err);
                    return callback("Error");
                } else{
                    if(data.lastErrorObject && data.lastErrorObject.updatedExisting) {
                        return callback(null, data.value._id);
                    } else {
                        return callback(null, data.lastErrorObject.upserted);
                    }
                }
            });
        },


        getDistinctValuesForKeyByCondition: function (keyName, findCond, callback) {
            if(!keyName || !callback && typeof callback !== "function"){
                console.log("Invalid model query");
                return false;
            }

            if(!findCond)
                findCond = {};

            return self.collection.distinct(keyName, findCond, callback);
        }

    }

};
