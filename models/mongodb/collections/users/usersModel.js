const self = this,
    _ = require("underscore"),
    baseModel = require("../../baseModel")(this),
    collectionName = 'users';


// Constructor, which takes in app, which helps initialise db from the common mongo db in app.js
let modelObj = {
    init: function(dbObj){
        self.collection = dbObj.db.bind(collectionName);
        self.ObjectID = dbObj.ObjectID;
    }

};

module.exports = function(dbObj) {
    let finalModel = _.extend(baseModel, modelObj);
    finalModel.init(dbObj);

    return finalModel;
};
