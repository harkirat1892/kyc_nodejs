const mongo = require('mongoskin');

let dbName = process.env.DB_NAME,
    dbUser = process.env.DB_USER,
    dbPassword = process.env.DB_PASSWORD,
    dbHost = process.env.DB_HOST || "localhost",
    dbPort = process.env.DB_PORT || "27017";


if(!dbName){
    console.log("Please specify DB_NAME in the environment");
    throw Error('DB_NAME not defined');
}

let dbConnection;


function getMongoDbConnection() {
    if (dbConnection){
        // If already there, return the same!
        return dbConnection;
    }

    // Connection not there, re-initialise
    let mongoUrl = "mongodb://" + dbHost + ":" + dbPort + "/" + dbName;

    if(dbUser && dbPassword && dbName) {
        mongoUrl = "mongodb://" + dbUser + ":" + dbPassword + "@" + dbHost + ":" + dbPort + "/" + dbName;
    }

    dbConnection = mongo.db(mongoUrl, {native_parser: true});


    // @TODO: use ReplSetServers later
    //var ReplSetServers = mongo.ReplSetServers;

    //var replSet = new ReplSetServers([
    //        new Server('localhost', 27017)
    //]);

    //var db = new Db(dbName', replSet, {w:0, native_parser: (process.env['TEST_NATIVE'] != null)});

    return dbConnection;
}


module.exports = {
    db: getMongoDbConnection(),
    ObjectID: mongo.ObjectID
};
